using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EcosystemAnalytics : MonoBehaviour {
    public enum DeathCause { Hunted, Starved, Dehydrated }

    // Singleton
    private static EcosystemAnalytics instance;

    // Stream para analíticas
    private StreamWriter writer;

    // Almacenamiento de estadísticas
    private Dictionary<string, int> deathsByHunt;
    private Dictionary<string, int> deathsByStarvation;
    private Dictionary<string, int> deathsByDehydration;

    // Separador a utilizar en las analíticas
    private static char separator;

    private void Awake() {
        /* Singleton */
        if (instance == null) {
            instance = this;
        } else if (!instance.Equals(this)) {
            Destroy(this);
        }
    }

    private void OnDestroy() {
        // Se asegura que el fichero de analíticas queda correctamente cerrado
        writer.Close();
    }

    public static void AddDeath(string className, DeathCause cause) {
        switch (cause) {
            case DeathCause.Hunted:
                instance.deathsByHunt[className]++;
                break;
            case DeathCause.Starved:
                instance.deathsByStarvation[className]++;
                break;
            case DeathCause.Dehydrated:
                instance.deathsByDehydration[className]++;
                break;
        }
    }

    public static void StartAnalytics(EcosystemElements elements, char sep) {
        // Inicialización del stream
        instance.writer = new StreamWriter(Application.dataPath + "/DataFiles/analytics.csv");
        instance.writer.AutoFlush = true;

        // Ajustes para la configuración del separador de fichero
        separator = sep;

        // Inicialización fichero analíticas
        System.Text.StringBuilder analytics = new System.Text.StringBuilder("Tick" + separator);
        // Cabeceras contadores
        foreach (ElementParameters ep in EcosystemController.ElementParents.Keys)
        {
            analytics.Append(ep.name + " count" + separator);
        }
        instance.deathsByDehydration = new Dictionary<string, int>();
        instance.deathsByHunt = new Dictionary<string, int>();
        instance.deathsByStarvation = new Dictionary<string, int>();
        foreach (string classN in elements.classNames) {
            if (!classN.Equals("Default")) {
                // Cabeceras
                analytics.Append(classN + " deaths by dehidration" + separator + classN + " deaths by hunt" + separator + classN + " deaths by starvation" + separator);
                // Estadísticas
                instance.deathsByDehydration.Add(classN, 0);
                instance.deathsByHunt.Add(classN, 0);
                instance.deathsByStarvation.Add(classN, 0);
            }
        }
        instance.writer.WriteLineAsync(analytics.ToString());
    }

    public static void WriteTickAnalytics(EcosystemElements elements) {
        System.Text.StringBuilder analytics = new System.Text.StringBuilder(EcosystemController.TickCount.ToString() + separator);
        foreach (ElementParameters ep in EcosystemController.ElementParents.Keys) {
            analytics.Append(EcosystemController.ElementParents[ep].childCount.ToString() + separator);
        }
        foreach (string classN in elements.classNames) {
            if (!classN.Equals("Default")) {
                analytics.Append(instance.deathsByDehydration[classN].ToString() + separator);
                analytics.Append(instance.deathsByHunt[classN].ToString() + separator);
                analytics.Append(instance.deathsByStarvation[classN].ToString() + separator);
            }
        }
        //analytics.Remove(analytics.Length - 1, 1);
        instance.writer.WriteLineAsync(analytics.ToString());

        // Analíticas nuevo tick
        foreach (string classN in elements.classNames) {
            if (!classN.Equals("Default")) {
                // Estadísticas
                instance.deathsByDehydration[classN] = 0;
                instance.deathsByHunt[classN] = 0;
                instance.deathsByStarvation[classN] = 0;
            }
        }
    }
}