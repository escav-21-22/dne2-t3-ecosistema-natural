using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DNE2/Element Properties", order = 2)]
public class ElementProperties : ScriptableObject {
    public ElementType type;

    [Header("Eatable")]
    public bool isEatable;
    public int nutritionalValue;

    [Header("Consumer")]
    public bool isConsumer;
    public int maxHungerBeforeStarve;
    public int eatThreshold;
    public int hungerIncrementPerTick;
    public ElementType eatPreference;

    [Header("Reproducible")]
    public bool isReproducible;
    public int reproductionFrequency;
    public int parentsCount;
    public int childrenCount;
}

[System.Serializable]
public enum ElementType {
    Plant, Herbivorous, Carnivorous
}
