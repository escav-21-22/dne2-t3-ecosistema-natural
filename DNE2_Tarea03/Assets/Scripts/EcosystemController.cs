using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EcosystemController : MonoBehaviour {
    // Singleton
    private static EcosystemController instance;

    [Header("References")]
    public EcosystemElements elements;
    [Space]
    public Transform parentPrefab;
    public GameObject elementPrefab;
    public GameObject desertBiomePrefab;
    public GameObject meadowBiomePrefab;
    public GameObject snowBiomePrefab;
    public GameObject waterBiomePrefab;

    // Segundos transcurridos desde el último tick ejecutado
    private float timeSinceLastTick;

    // Contador de ticks
    private int tickCount;

    // Stream para configuración
    private StreamReader reader;

    // Diccionario para el estado del sueño de cada tipo de elemento
    private Dictionary<ElementParameters, bool> sleepTimes;

    // Diccionario para los padres de cada tipo de elemento
    private Dictionary<ElementParameters, Transform> elementParents;

    // Diccionario para los contadores de creación de cada tipo de elemento
    private Dictionary<ElementParameters, int> elementCreated;

    public char separator;

    private string aux;

    #region Getters/Setters

    /// <summary>
    /// Número de ticks ejecutados hasta el momento
    /// </summary>
    public static int TickCount {
        get {
            return instance.tickCount;
        }
    }

    /// <summary>
    /// Relación de las distintas clases de elementos
    /// </summary>
    public static Dictionary<ElementParameters, Transform> ElementParents {
        get {
            return instance.elementParents;
        }
    }

    #endregion

    #region Unity Event Functions

    private void Awake() {
        /* Singleton */
        if (instance == null) {
            instance = this;
        } else if (!instance.Equals(this)) {
            Destroy(this);
        }
    }

    private void Start() {
        // Inicialización de los streams
        reader = new StreamReader(Application.dataPath + "/DataFiles/parameters.csv");

        // Configuración de parámetros para los diferentes elementos del ecosistema
        List<string[]> rows = CSVSerializer.ParseCSV(reader.ReadToEnd().Replace(separator, ','));
        if (rows != null) {
            elements.parameters = new List<ElementParameters>(CSVSerializer.Deserialize<ElementParameters>(rows));
        }

        // Inicialización del ecosistema
        PrepareEcosytem();
        EcosystemAnalytics.StartAnalytics(elements, separator);
    }

    private void Update() {
        timeSinceLastTick += Time.deltaTime;
        if (timeSinceLastTick >= elements.tickDuration) {
            // Tick
            tickCount++;
            // Notifica la ejecución del tick a los elementos del ecosistema
            foreach (ElementParameters param in elementParents.Keys) {
                foreach (Transform element in elementParents[param]) {
                    element.GetComponent<ElementBrain>().OnTick();
                }
            }
            // Preparación para el siguiente tick
            timeSinceLastTick = 0;
            // Actualización de los padres
            UpdateParentsName();
            // Analíticas
            EcosystemAnalytics.WriteTickAnalytics(elements);
        }
    }    

    private void OnDestroy() {
        // Se asegura que el fichero de configuración queda correctamente cerrado
        reader.Close();
    }

    #endregion

    #region Public Static Functions

    public static List<ElementBrain> CreateElements(ElementParameters parameters, int count) {
        return instance.CreateElementsOfClass(parameters, count);
    }

    public static ElementBrain GetElementToEat(ElementType type, Vector3 position, int distance) {
        // Busca un elemento de la clase indicada, asegurándose que cumple la distancia de caza
        ElementBrain res;
        int tries = 20;
        do {
            res = instance.FindElementOfType(type);
            tries--;
        } while ((tries > 0) && (res != null) && (Vector3.Distance(res.transform.position, position) > distance));
        // Se asegura que el elemento encontrado es válido
        if ((res != null) && (Vector3.Distance(res.transform.position, position) > distance))
        {
            res = null;
        }

        return res;
    }

    public static ElementBrain GetElementToReproduce(string elementName, ElementBrain element) {
        // Busca un elemento de la clase indicada, asegurándose que no coincide con el indicado
        ElementBrain res;
        int tries = 10;
        do {
            res = instance.FindElementOfClass(elementName);
            tries--;
        } while ((element.Equals(res)) && (tries > 0));

        return res;
    }

    public static Vector3? GetWater(Vector3 position, int distance)
    {
        // Busca un bioma de agua, asegurándose que cumple la distancia de bebida
        Transform water = null;
        int tries = 20;
        do
        {
            water = instance.transform.GetChild(Random.Range(0, instance.transform.childCount));
            tries--;
        } while ((tries > 0) && ((!water.name.Equals("Water")) || (Vector3.Distance(water.transform.position, position) > distance)));
        // Se asegura que el biaoma de agua encontrado es válido
        Vector3? res = null;
        if ((water != null) && (water.name.Equals("Water")) && (Vector3.Distance(water.transform.position, position) <= distance))
        {
            res = water.position;
        }

        return res;
    }

    #endregion

    private void BiomeCreation() {
        int desertAux = elements.desertCount;
        int meadowAux = elements.meadowCount;
        int snowAux = elements.snowCount;
        int waterAux = elements.waterCount;
        // Generación de las casillas
        int rnd;
        for (int x = 0; x < elements.size.x; x++) {
            for (int z = 0; z < elements.size.y; z++) {
                rnd = Random.Range(0, desertAux + meadowAux + snowAux + waterAux);
                if (rnd < desertAux) {
                    Instantiate(desertBiomePrefab, new Vector3(x, 0, z), Quaternion.identity, transform).name = "Desert";
                    desertAux--;
                } else if (rnd < desertAux + meadowAux) {
                    Instantiate(meadowBiomePrefab, new Vector3(x, 0, z), Quaternion.identity, transform).name = "Meadow";
                    meadowAux--;
                } else if (rnd < desertAux + meadowAux + snowAux) {
                    Instantiate(snowBiomePrefab, new Vector3(x, 0, z), Quaternion.identity, transform).name = "Snow";
                    snowAux--;
                } else {
                    Instantiate(waterBiomePrefab, new Vector3(x, 0, z), Quaternion.identity, transform).name = "Water";
                    waterAux--;
                }
            }
        }
    }

    private ElementBrain CreateElement(ElementParameters parameters) {
        // Intanciación del elemento
        ElementBrain brain = Instantiate(elementPrefab, new Vector3(Random.Range(0, elements.size.x), 0, Random.Range(0, elements.size.y)), Quaternion.identity, elementParents[parameters]).GetComponent<ElementBrain>();
        int index = elements.classNames.IndexOf(parameters.name);
        if (index < 1) {
            index = 0;
        }
        Instantiate(elements.classPrefabs[index], brain.transform).transform.localPosition = new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.2f, 0.6f), Random.Range(0.0f, 1.0f));
        // Configuración del elemento
        brain.parameters = parameters;
        brain.SetID(parameters.name + elementCreated[parameters].ToString());
        ++elementCreated[parameters];

        return brain;
    }

    private List<ElementBrain> CreateElementsOfClass(ElementParameters parameters, int count) {
        // Crea los elementos de la clase indicada
        List<ElementBrain> res = new List<ElementBrain>();
        for (int e = 0; e < count; e++) {
            res.Add(CreateElement(parameters));
        }

        return res;
    }

    private ElementBrain FindElementOfClass(string className) {
        // Busca entre los elementos registrados en el ecosistema
        ElementBrain res = null;
        List<ElementBrain> candidates = new List<ElementBrain>();
        foreach (ElementParameters param in elementParents.Keys) {
            // Comprueba si son de la clase que se busca
            if (param.name.Equals(className)) {
                foreach (Transform element in elementParents[param]) {
                    candidates.Add(element.GetComponent<ElementBrain>());
                }
            }
        }
        // Se escoge aleatoriamente un elemento entre los elementos candidatos
        if (candidates.Count > 0) {
            res = candidates[Random.Range(0, candidates.Count)];
        }

        return res;
    }

    private ElementBrain FindElementOfType(ElementType type) {
        // Busca entre los elementos registrados en el ecosistema
        ElementBrain res = null;
        List<ElementBrain> candidates = new List<ElementBrain>();
        foreach (ElementParameters param in elementParents.Keys) {
            // Comprueba si son de la clase que se busca
            if (param.type.Equals(type)) {
                foreach (Transform element in elementParents[param]) {
                    candidates.Add(element.GetComponent<ElementBrain>());
                }
            }
        }
        // Se escoge aleatoriamente un elemento entre los elementos candidatos
        if (candidates.Count > 0) {
            res = candidates[Random.Range(0, candidates.Count)];
        }

        return res;
    }

    private void PrepareEcosytem() {
        // Inicialización de las variables
        elementParents = new Dictionary<ElementParameters, Transform>();
        elementCreated = new Dictionary<ElementParameters, int>();
        // Creación de los biomas
        BiomeCreation();
        // Creación de los agrupadores de elementos
        foreach (ElementParameters elemParams in elements.parameters) {
            elementParents.Add(elemParams, Instantiate(parentPrefab, Vector3.zero, Quaternion.identity));
            elementCreated.Add(elemParams, 0);
            CreateElementsOfClass(elemParams, elemParams.initialcount);
        }
        // Actualización del nombre de los agrupadores
        UpdateParentsName();
    }

    private void UpdateParentsName()
    {
        foreach (ElementParameters elemParams in elementParents.Keys)
        {
            elementParents[elemParams].name = elemParams.name + " (" + elementParents[elemParams].childCount + ")";
        }
    }
}