using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementBrain : MonoBehaviour {
    //public ElementProperties properties;
    public ElementParameters parameters;
    public string ID;

    private int currentHunger;
    private int currentThirst;

    private int lastReproductionTick;

    private List<string> parents;
    private List<string> Parents {
        get {
            return parents;
        }
        set {
            parents = value;
            if (parents.Count > parameters.inbreedingdisease) {
                parents.RemoveRange(parameters.inbreedingdisease, parents.Count - parameters.inbreedingdisease);
            }
        }
    }

    private bool geneticDisease;
    private int transientDisease;

    public void SetID(string id) {
        ID = id;
        name = ID;
        lastReproductionTick = EcosystemController.TickCount;
        parents = new List<string>();
    }

    public void OnTick() {

        // Reproducción
        if (parameters.IsReproducible) {
            // Comprobación de la frecuencia de reproducción
            if (EcosystemController.TickCount - lastReproductionTick >= parameters.reproductionfrequency) {
                TryToReproduce();
            }
        }

        // Enfermedades
        if (geneticDisease) {
            currentHunger += parameters.hungerincrementdisease;
        }
        if (transientDisease > 0) {
            currentHunger += parameters.hungerincrementdisease;
            --transientDisease;
        } else if (Random.Range(0, 100) < parameters.transientdiseaseprobability) {
            transientDisease = parameters.transientdiseaseduration;
        }

        // Alimentación & sed
        if (parameters.IsConsumer) {
            // Incremento
            currentHunger += parameters.hungerincrementpertick;
            currentThirst += parameters.thirstincrementpertick;
            // Muerte
            if (currentHunger >= parameters.maxhungerbeforestarve) {
                Kill(EcosystemAnalytics.DeathCause.Starved);
            } else if (currentThirst >= parameters.maxthirstbeforedehydration) {
                Kill(EcosystemAnalytics.DeathCause.Dehydrated);
            } else {
                // Búsqueda de comida
                if (currentHunger >= parameters.eatthreshold) {
                    SearchFood();
                }
                // Búsqueda de agua
                if (currentThirst >= parameters.drinkthreshold) {
                    SearchWater();
                }
            }
        }
    }

    public void Kill(EcosystemAnalytics.DeathCause cause) {
        transform.parent = null;
        EcosystemAnalytics.AddDeath(parameters.name, cause);
        Destroy(gameObject);

    }

    public void SearchFood() {
        // Busca el alimento
        ElementBrain food = EcosystemController.GetElementToEat(parameters.eatpreference, transform.position, parameters.huntmaxdistance);
        if (food != null) {
            // Ha encontrado el alimento, calcula la probabilidad de comérselo
            int prob = food.parameters.probabilitytobeeaten;
            if ((food.geneticDisease) || (food.transientDisease > 0)) {
                prob += food.parameters.eatenprobmodifwhendisease;
            }
            if ((geneticDisease) || (transientDisease > 0)) {
                prob += parameters.consumeprobmodifwhendisease;
            }
            if (Random.Range(0, 100) < prob) {
                // Logra la comida
                // Debug.Log(name + " se come a " + food.name);
                MoveTo(food.transform.position);
                currentHunger -= food.parameters.nutritionalvalue;
                currentHunger = Mathf.Max(0, currentHunger);
                // Transmisión de enfermedades
                if (food.transientDisease > 0) {
                    transientDisease = parameters.transientdiseaseduration;
                }
                food.Kill(EcosystemAnalytics.DeathCause.Hunted);
            }
        }
    }

    public void SearchWater() {
        // Busca el agua
        Vector3? res = EcosystemController.GetWater(transform.position, parameters.watermaxdistance);
        if (res != null)
        {
            currentThirst = 0;
            MoveTo((Vector3) res);
        }
    }

    private void MoveTo(Vector3 newPosition)
    {
        Vector3 aux = transform.position;
        aux.x = newPosition.x;
        aux.z = newPosition.z;
        transform.position = aux;
    }

    private void TryToReproduce() {
        // Busca la pareja para la reproducción
        ElementBrain pair = EcosystemController.GetElementToReproduce(parameters.name, this);
        if (pair != null) {
            if (EcosystemController.TickCount - pair.lastReproductionTick >= parameters.reproductionfrequency) {
                // Ha encontrado pareja, se reproduce
                // Debug.Log(name + " se reproduce con " + pair.name);
                List<ElementBrain> children = EcosystemController.CreateElements(parameters, parameters.childrencount);
                lastReproductionTick = EcosystemController.TickCount;
                pair.lastReproductionTick = EcosystemController.TickCount;
                if (parameters.inbreedingdisease > 0) {
                    // Relación de antepasados parientes
                    HashSet<string> auxParents = new HashSet<string>(parents);
                    for (int p = 0; p < pair.Parents.Count; p++) {
                        auxParents.Add(pair.Parents[p]);
                    }
                    foreach (ElementBrain c in children) {
                        c.Parents = new List<string>() { name, pair.name };
                        c.Parents.AddRange(auxParents);
                    }
                    // Transmisión de enfermedades
                    if ((geneticDisease) || (pair.geneticDisease)) {
                        foreach (ElementBrain c in children) {
                            c.geneticDisease = (Random.Range(0, 100) < parameters.geneticdiseaseprobability);
                        }
                    } else if (auxParents.Count < (parents.Count + pair.Parents.Count)) {
                        foreach (ElementBrain c in children) {
                            c.geneticDisease = true;
                        }
                    }
                }
            }
        }
    }

}