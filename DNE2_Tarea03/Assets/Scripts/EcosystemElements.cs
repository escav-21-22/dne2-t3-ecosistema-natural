using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DNE2/Ecosystem Elements", order = 1)]
public class EcosystemElements : ScriptableObject {

    [Header("Ecosystem Details")]
    public Vector2Int size;
    [Space]
    public int desertCount;
    public int meadowCount;
    public int snowCount;
    public int waterCount;
    [Space]
    public float tickDuration;

    public List<ElementParameters> parameters;

    public List<string> classNames;

    public List<GameObject> classPrefabs;
}

[System.Serializable]
public class ElementParameters {
    public string name;
    public ElementType type;
    public int initialcount;

    [Header("Eatable")]
    public int eatable;
    public bool IsEatable {
        get {
            return (eatable == 1);
        }
    }
    public int nutritionalvalue;
    public int probabilitytobeeaten;
    public int eatenprobmodifwhendisease;

    [Header("Consumer")]
    public int consumer;
    public bool IsConsumer {
        get {
            return (consumer == 1);
        }
    }
    public int maxhungerbeforestarve;
    public int maxthirstbeforedehydration;
    public int eatthreshold;
    public int drinkthreshold;
    public int hungerincrementpertick;
    public int thirstincrementpertick;
    public ElementType eatpreference;
    public int consumeprobmodifwhendisease;
    public int huntmaxdistance;
    public int watermaxdistance;

    [Header("Reproducible")]
    public int reproducible;
    public bool IsReproducible {
        get {
            return (reproducible == 1);
        }
    }
    public int reproductionfrequency;
    public int childrencount;
    public int inbreedingdisease;

    [Header("Day/Night")]
    public int timeToSleep;
    public int hoursToSleep;

    [Header("Diseases")]
    public int transientdiseaseprobability;
    public int transientdiseaseduration;
    public int geneticdiseaseprobability;
    public int hungerincrementdisease;

}